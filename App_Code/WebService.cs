﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService {
    dboperation db = new dboperation();
   
    public WebService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public string reg(string nm, string age, string dob, string gen, string addr, string mail, string ph,string pass)
    {
        string s = "";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select max(user_id) from user_reg";
        int id = db.max_id(cmd);

        cmd.CommandText = "insert into user_reg values('" + id + "','" + nm + "','" + age + "','" + dob + "','" + gen + "','" + addr + "','" + mail + "','" + ph + "')";
        SqlCommand cm = new SqlCommand();
        cm.CommandText = "insert into login values('"+id+"','"+mail+"','"+pass+"','user')";
        try
        {
            db.execute(cmd);
            db.execute(cm);
            s = "success";
        }
        catch
        {
            s = "error";
        }
        return s;
    }


    [WebMethod]
    public string login(string u, string p)
    {
        string s = "";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select * from login where user_name='" + u + "' and password='" + p + "' and type='user'";
        DataTable dt = db.view(cmd);
        if (dt.Rows.Count > 0)
        {
            s = dt.Rows[0][0].ToString();
        }
        else
        {
            s = "error";
        }
        return s;
   }

    [WebMethod]
    public string search_product(string c)
    {
        string s = "";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select * from product where category_id='"+c+"'";
        DataTable dt = db.view(cmd);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                s += dr[0].ToString() + "#" + dr[1].ToString() + "#" + dr[3].ToString() + "#" + dr[4].ToString() + "@";
            }
        }
        else
        {
            s = "error";
        }
        return s;
    }

    [WebMethod]
    public string search_category()
    {
        string s = "";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select * from category";
        DataTable dt = db.view(cmd);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                s += dr[0].ToString() + "-" + dr[1].ToString() + "@";
            }
        }
        else
        {
            s = "error";
        }
        return s;
    }


    [WebMethod]
    public string cart(string p_id,string u_id,string qty,string prc)
    {
        string s = "";
        SqlCommand cmd = new SqlCommand();

        cmd.CommandText = "select max(cart_id) from cart";
        int id = db.max_id(cmd);

        cmd.CommandText="select * from order_tbl where user_id='"+u_id+"' and  status='pending'";
        DataTable dt = db.view(cmd);
        if (dt.Rows.Count > 0)
        {
            string or_id = dt.Rows[0][0].ToString();
            cmd.CommandText = "update order_tbl set amount+='" + prc + "' where order_id='" + or_id + "'";
            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandText = "insert into cart values('" + id + "','" + p_id + "','" + u_id + "','" + or_id + "','" + qty + "','" + prc + "')";
            try
            {
                db.execute(cmd);
                db.execute(cmd1);
                s = "success";
            }
            catch
            {
                s = "error";
            }
        }
        else
        {
            cmd.CommandText = "select max(order_id) from order_tbl";
            int or_id = db.max_id(cmd);
            cmd.CommandText = "insert into order_tbl values('" + or_id + "','" + u_id + "','" + DateTime.Now.ToShortDateString() + "','" + prc + "','pending')";
            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandText = "insert into cart values('" + id + "','" + p_id + "','" + u_id + "','" + or_id + "','" + qty + "','" + prc + "')";
            try
            {
                db.execute(cmd);
                db.execute(cmd1);
                s = "success";
            }
            catch
            {
                s = "error";
            }
        }
        return s;
    }


    [WebMethod]
    public string view_cart(string u_id)
    {
        string s = "";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT cart.order_id, product.product_name, cart.quantity, cart.price FROM cart INNER JOIN order_tbl ON cart.order_id = order_tbl.order_id INNER JOIN product ON cart.product_id = product.product_id WHERE  (order_tbl.status = 'pending') AND (order_tbl.user_id = '" + u_id + "')";
        DataTable dt = db.view(cmd);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                s += dr[0].ToString() + "#" + dr[1].ToString() +"#"+dr[2].ToString() + "#"+dr[3].ToString()+"@";
            }
        }
        else
        {
            s = "error";
        }
        return s;
    }


    [WebMethod]
    public string confirm_cart(string o_id)
    {
       string s = "";
       SqlCommand cmd = new SqlCommand();
       cmd.CommandText = "select * from cart where order_id='"+o_id+"'";
       DataTable dt = db.view(cmd);
       if (dt.Rows.Count > 0)
       {
           foreach (DataRow dr in dt.Rows)
           {
               int qty =Convert.ToInt32( dr[4].ToString());
               SqlCommand cm = new SqlCommand();
               cm.CommandText = "update product set quantity =quantity-"+qty+" where product_id="+dr[1].ToString()+"";
               db.execute(cm);
           }
       }

       cmd.CommandText = "update order_tbl set status='confirmed' where order_id='" + o_id + "'";

       try
       {
           db.execute(cmd);
           s = "success";
       }
       catch
       {
           s = "Error";
       }
       return s;
    }


    [WebMethod]
    public string feedbck(string p_id,string u_id,string rvw)
    {
        string s = "";
        SqlCommand cmd = new SqlCommand();
         cmd.CommandText = "select max(review_id) from review";
        int id = db.max_id(cmd);
        cmd.CommandText = "insert into review values('" + id + "','" + p_id + "','" + u_id + "','" + rvw + "','" + DateTime.Now.ToShortDateString() + "')";
        try
        {
            db.execute(cmd);
            s = "success";
        }
        catch
        {
            s = "Error";
        }
        return s;
    }


    [WebMethod]
    public string view_dlvry(string uid)
    {
        string s = "";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT product.product_name, cart.cart_id, cart.price, order_tbl.date, order_tbl.amount,order_tbl.status FROM cart INNER JOIN order_tbl ON cart.order_id = order_tbl.order_id INNER JOIN product ON cart.product_id = product.product_id WHERE  (order_tbl.status = 'delivered') AND order_tbl.user_id='" + uid + "'";
        DataTable dt = db.view(cmd);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                s += dr[0].ToString() + "#" + dr[1].ToString() + "#" + dr[2].ToString() + "#" + dr[3].ToString() + "#" + dr[4].ToString() + "#" + dr[5].ToString() + "@";
            }
        }
        else
        {
            s = "error";
        }
        return s;
    }



    [WebMethod]
    public string dlt(string cid)
    {
        string s = "";
        SqlCommand cmd = new SqlCommand();
       
        cmd.CommandText = "delete from cart where cart_id="+cid+"";
        try
        {
            db.execute(cmd);
            s = "success";
        }
        catch
        {
            s = "Error";
        }
        return s;
    }
    [WebMethod]
    public string getPurchasedProduct(string uid)
    {
        string s = "";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT distinct c.product_id,p.product_name from cart c,product p,order_tbl o where c.product_id=p.product_id and c.user_id=o.user_id and c.user_id="+uid+" and o.status='delivered'";
        DataTable dt = db.view(cmd);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                s += dr[0].ToString() + "-" + dr[1].ToString() + "#";
            }
        }
        else
        {
            s = "error";
        }
        return "-Select-#" + s;
    }
    [WebMethod]
    public string view_reviews(string product_id)
    {
        string s = "";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select review,date from review where product_id='"+product_id+"'";
        DataTable dt = db.view(cmd);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                s += dr[0].ToString() + "#" + dr[1].ToString() + "@";
            }
        }
        else
        {
            s = "error";
        }
        return s;

    }

    [WebMethod]
    public string review_summary(string product_id)
    {
        string res = "";
        List<string> kw = new List<string>();
        int flag = 0;
        int pos = 0;
        int neg = 0;
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select review from  review where product_id='" + product_id + "'";
        DataTable dt = db.view(cmd);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                string data = dr[0].ToString().Replace(".", " ");
                string[] word = data.Split(' ');
                for (int i = 0; i < word.Count(); i++)
                {
                    SqlCommand cmd1 = new SqlCommand();
                    cmd.CommandText = "select stem_word from stem";
                    DataTable dt1 = db.view(cmd);
                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        if (dr1[0].ToString() != word[i])
                        {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 1)
                    {
                        kw.Add(word[i]);

                    }
                }
                SqlCommand cmd2 = new SqlCommand();
                cmd2.CommandText = "select plsa,value from plsa";
                DataTable dt4 = db.view(cmd2);
                int val = 0;
                foreach (string str in kw)
                {
                    foreach (DataRow dr4 in dt4.Rows)
                    {
                        if (str == dr4[0].ToString())
                        {
                            val += Convert.ToInt32(dr4[1].ToString());
                            break;

                        }
                    }
                }
                if (val > 0)
                {
                    pos++;
                }
                else
                {
                    neg++;
                }


            }
            if (pos > neg)
            {
                res = "Good";
            }
            else if (pos < neg)
            {
                res = "Bad";

            }
            else
            {
                res = "Average quality";
            }
        }
        else
        {
            res = "Not reviewed yet!!";
        }
        return res;
    }

    [WebMethod]
    public string review_summary_live(string prod)
    {
        string res = "";
        List<string> kw = new List<string>();
        int flag = 0;
        int pos = 0;
        int neg = 0;
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select review from  liveReview where revProd='" + prod + "'";
        DataTable dt = db.view(cmd);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                string data = dr[0].ToString().Replace(".", " ");
                string[] word = data.Split(' ');
                for (int i = 0; i < word.Count(); i++)
                {
                    SqlCommand cmd1 = new SqlCommand();
                    cmd.CommandText = "select stem_word from stem";
                    DataTable dt1 = db.view(cmd);
                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        if (dr1[0].ToString() != word[i])
                        {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 1)
                    {
                        kw.Add(word[i]);

                    }
                }
                SqlCommand cmd2 = new SqlCommand();
                cmd2.CommandText = "select plsa,value from plsa";
                DataTable dt4 = db.view(cmd2);
                int val = 0;
                foreach (string str in kw)
                {
                    foreach (DataRow dr4 in dt4.Rows)
                    {
                        if (str == dr4[0].ToString())
                        {
                            val += Convert.ToInt32(dr4[1].ToString());
                            break;

                        }
                    }
                }
                if (val > 0)
                {
                    pos++;
                }
                else
                {
                    neg++;
                }


            }
            if (pos > neg)
            {
                res = "Good";
            }
            else if (pos < neg)
            {
                res = "Bad";

            }
            else
            {
                res = "Average quality";
            }
        }
        else
        {
            res = "Not reviewed yet!!";
        }
        return res;
    }

}
